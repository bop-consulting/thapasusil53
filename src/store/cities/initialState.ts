import { ICityType } from "@/types/CityType"

export interface CitiesState {
    allCities: ICityType[];
    loading: boolean
}

export const initialState: CitiesState = {
    allCities: [],
    loading: false
}
