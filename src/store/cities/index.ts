import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { initialState } from "./initialState";

export const citiesSlice = createSlice({
    name: 'cities',
    initialState,
    reducers: {
        setAllCities: (state, { payload }: PayloadAction<any>) => {
            state.allCities = payload;
        },
        setLoading: (state, { payload }: PayloadAction<any>) => {
            state.loading = payload;
        }
    }
});

export const {
    setAllCities,
    setLoading
} = citiesSlice.actions;

export default citiesSlice.reducer;