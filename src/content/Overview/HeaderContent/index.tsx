import React from "react";

import {
  Box,
  Button,
  Grid,
  Typography,
  styled,
  Autocomplete,
  TextField,
} from "@mui/material";
import { pink } from "@mui/material/colors";

import { CityType, DemoContext } from "src/contexts/DemoContext";
import { useSelector } from "@/store";
import { ICityType } from "@/types/CityType";

const TypographyPrimary = styled(Typography)(
  ({ theme }) => `
      color: ${theme.colors.alpha.trueWhite[100]};
`
);

const UnlockButton = styled(Button)(
  ({ theme }) => `
      background-color:${pink[400]};
      color: ${theme.colors.alpha.trueWhite[100]};
      text-transform:uppercase;
      border-radius: ${theme.spacing(11)};
      padding-left:${theme.spacing(2.5)};
      padding-right:${theme.spacing(2.5)};
      &:hover{
        background-color:${pink[300]};
      }
`
);

export const CITIES: CityType[] = [
  {
    name: "London",
    subTitle: "United Kingdom",
    latitude: 51.5074,
    longitude: -0.1278,
  },
  {
    name: "San Francisco",
    subTitle: "United States",
    latitude: 37.7751,
    longitude: -122.4193,
  },
  {
    name: "New York",
    subTitle: "United States",
    latitude: 40.7128,
    longitude: -74.006,
  },
  {
    name: "Paris",
    subTitle: "France",
    latitude: 48.8566,
    longitude: 2.3522,
  },
];

function HeaderContent() {
  const { allCities, loading } = useSelector((state) => state.cities);

  const { selectedCities, handleCitySelect } = React.useContext(DemoContext);

  const handleChange = (_event: React.SyntheticEvent, value: ICityType[]) => {
    handleCitySelect(value);
  };

  return (
    <Grid
      container
      spacing={2}
      alignItems="center"
      justifyContent="space-between"
    >
      <Grid item xs={4} lg={5}>
        <Autocomplete
          multiple
          freeSolo
          sx={{
            m: 0,
          }}
          options={allCities}
          getOptionLabel={(option) => option?.city_name}
          value={selectedCities}
          loading={loading}
          onChange={handleChange}
          renderInput={(params) => (
            <TextField
              {...params}
              fullWidth
              label="Location"
              variant="outlined"
              placeholder={"Select location..."}
            />
          )}
          renderOption={(props, option) => {
            return (
              <Box sx={{ padding: "0.1rem" }}>
                <Typography {...props}>
                  {option?.city_name} | {option?.state_name} |{" "}
                  {option?.country_name}
                </Typography>
              </Box>
            );
          }}
        />
      </Grid>
      <Grid
        item
        container
        xs={8}
        lg={6}
        justifyContent="space-between"
        alignItems="center"
      >
        <Box display="flex" sx={{ mx: 1 }}>
          <TypographyPrimary
            variant="h3"
            sx={(theme) => ({
              fontSize: `${theme.typography.pxToRem(14)}`,
              display: "flex",
              alignItems: "baseline",
              mr: 4,
            })}
          >
            BOP
            <TypographyPrimary
              variant="h3"
              sx={(theme) => ({
                fontSize: `${theme.typography.pxToRem(12)}`,
                mr: 1,
              })}
            >
              500
            </TypographyPrimary>
            Cities
          </TypographyPrimary>
          <TypographyPrimary
            variant="h3"
            sx={(theme) => ({
              fontSize: `${theme.typography.pxToRem(14)}`,
              mr: 4,
            })}
          >
            Xxxxxxx
          </TypographyPrimary>
          <TypographyPrimary
            variant="h3"
            sx={(theme) => ({
              fontSize: `${theme.typography.pxToRem(14)}`,
              mr: 4,
            })}
          >
            Xxxxxxx
          </TypographyPrimary>
        </Box>
        <UnlockButton>Unlock City Data</UnlockButton>
      </Grid>
    </Grid>
  );
}

export default HeaderContent;
