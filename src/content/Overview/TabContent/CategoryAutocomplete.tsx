import React from "react";
import { Box, useTheme, Button } from "@mui/material";

const CATEGORIES: string[] = [
  "Cultural Facilities 12",
  "Heritage Sites 1",
  "Accessibility to Culture 12",
  "Vibrancy 10",
  "Culture and Creative Education 5",
  "Creative Employment 7",
  "Innovation in Creative Industries 4",
];

function CategoryAutocomplete() {
  const theme = useTheme();
  return (
    <Box
      m={2}
      p={1}
      sx={{
        backgroundColor: theme.colors.alpha.black[10],
        borderRadius: 1,
      }}
    >
      {CATEGORIES.map((category, index) => (
        <Button
          key={index + category}
          color="secondary"
          variant="outlined"
          sx={{ m: theme.typography.pxToRem(4) }}
        >
          {category}
        </Button>
      ))}
    </Box>
  );
}

export default CategoryAutocomplete;
