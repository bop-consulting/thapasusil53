import { Grid } from "@mui/material";

import Block1 from "src/content/Blocks/ChartsLarge/Block1";
import Block2 from "src/content/Blocks/ChartsLarge/Block2";
import Block3 from "src/content/Blocks/ChartsLarge/Block3";
import Block4 from "src/content/Blocks/ChartsLarge/Block4";
import Block5 from "src/content/Blocks/ChartsLarge/Block5";
import Block6 from "src/content/Blocks/ChartsLarge/Block6";
import Block7 from "src/content/Blocks/ChartsLarge/Block7";
import Block8 from "src/content/Blocks/ChartsLarge/Block8";

function ParisTabContent() {
  return (
    <>
      <Grid
        sx={{
          px: 4,
        }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={4}
      >
        <Grid item md={6} xs={12}>
          <Block1 />
        </Grid>
        <Grid item md={6} xs={12}>
          <Block2 />
        </Grid>
        <Grid item md={6} xs={12}>
          <Block3 />
        </Grid>
        <Grid item md={6} xs={12}>
          <Block4 />
        </Grid>
        <Grid item md={6} xs={12}>
          <Block5 />
        </Grid>
        <Grid item md={6} xs={12}>
          <Block6 />
        </Grid>
        <Grid item md={6} xs={12}>
          <Block7 />
        </Grid>
        <Grid item md={6} xs={12}>
          <Block8 />
        </Grid>
      </Grid>
    </>
  );
}

export default ParisTabContent;
