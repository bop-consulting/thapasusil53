import {
  ChangeEvent,
  useState,
  useCallback,
  useEffect,
  useContext,
} from "react";
import { Grid, Tab, Tabs, Card, Box, styled, useTheme } from "@mui/material";

import { useRefMounted } from "src/hooks/useRefMounted";
import type { Product } from "src/models/product";
import { productsApi } from "src/mocks/products";
import { DemoContext } from "src/contexts/DemoContext";
import TasksAnalytics from "src/content/Dashboards/Tasks/TasksAnalytics";
import Performance from "src/content/Dashboards/Tasks/Performance";

import LondonTabContent from "./LondonTabContent";
import NewYorkTabContent from "./NewYorkTabContent";
import ParisTabContent from "./ParisTabContent";
import CategoryAutocomplete from "./CategoryAutocomplete";
import CityComparisonComponent from "./CityComparisonComponent";

const TabsContainerWrapper = styled(Box)(
  ({ theme }) => `
      padding: 0 ${theme.spacing(8)};
      position: relative;
      bottom: -1px;
      margin-top: 3rem;

      .MuiTabs-root {
        height: 48px;
        min-height: 48px;
      }

      .MuiTabs-scrollableX {
        overflow-x: auto !important;
      }

      .MuiTabs-indicator {
          min-height: 4px;
          height: 4px;
          box-shadow: none;
          background: none;
          border: 0;

          &:after {
            position: absolute;
            left: 50%;
            width: 28px;
            content: ' ';
            margin-left: -14px;
            background: ${theme.colors.primary.main};
            border-radius: inherit;
            height: 100%;
          }
         
      }

      .MuiTab-root {
          &.MuiButtonBase-root {
              height: 44px;
              min-height: 44px;
              background: ${theme.colors.alpha.white[50]};
              border: 1px solid ${theme.colors.alpha.black[10]};
              border-bottom: 0;
              position: relative;
              margin-right: ${theme.spacing(1)};
              font-size: ${theme.typography.pxToRem(14)};
              color: ${theme.colors.alpha.black[80]};
              border-bottom-left-radius: 0;
              border-bottom-right-radius: 0;

              .MuiTouchRipple-root {
                opacity: .1;
              }

              &:after {
                position: absolute;
                left: 0;
                right: 0;
                width: 100%;
                bottom: 0;
                height: 1px;
                content: '';
                background: ${theme.colors.alpha.black[10]};
              }

              &:hover {
                color: ${theme.colors.alpha.black[100]};
              }
          }

          &.Mui-selected {
              color: ${theme.colors.alpha.black[100]};
              background: ${theme.colors.alpha.white[100]};
              border-bottom-color: ${theme.colors.alpha.white[100]};

              &:after {
                height: 0;
              }
          }
      }
  `
);

function DemoOne() {
  const isMountedRef = useRefMounted();
  const theme = useTheme();

  const { selectedCities } = useContext(DemoContext);
  const [products, setProducts] = useState<Product[]>([]);
  const [currentTab, setCurrentTab] = useState<string>("");

  const handleTabsChange = (_event: ChangeEvent<{}>, value: string): void => {
    setCurrentTab(value);
  };

  const TAB_CONTENTS = [
    <LondonTabContent products={products} />,
    <NewYorkTabContent />,
    <ParisTabContent />,
    <Box
      p={4}
      sx={{
        background: `${theme.colors.alpha.black[5]}`,
      }}
    >
      <Grid container spacing={4}>
        <Grid item xs={12} sm={6} md={8}>
          <TasksAnalytics />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Performance />
        </Grid>
      </Grid>
    </Box>,
  ];

  const getProducts = useCallback(async () => {
    try {
      const response = await productsApi.getProducts();

      if (isMountedRef()) {
        setProducts(response);
      }
    } catch (err) {
      console.error(err);
    }
  }, [isMountedRef]);

  useEffect(() => {
    getProducts();
    selectedCities?.length
      ? setCurrentTab(selectedCities.at(-1).city_name)
      : setCurrentTab("");
  }, [getProducts, selectedCities]);

  return selectedCities.length && currentTab ? (
    <>
      <TabsContainerWrapper>
        <Tabs
          onChange={handleTabsChange}
          value={currentTab}
          variant="scrollable"
          scrollButtons="auto"
          textColor="primary"
          indicatorColor="primary"
        >
          {selectedCities.length > 1 && (
            <Tab
              key="CityComparison"
              label="City Comparison"
              value="CityComparison"
            />
          )}

          {selectedCities.map((tab) => (
            <Tab
              key={tab.city_name}
              label={tab.city_name}
              value={tab.city_name}
            />
          ))}
        </Tabs>
      </TabsContainerWrapper>
      <Card
        variant="outlined"
        sx={{
          mx: 4,
          mb: 4,
          position: "relative",
        }}
      >
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={0}
        >
          <Grid item xs={12} container>
            <Grid item xs={12} xl={10}>
              <CategoryAutocomplete />
            </Grid>
          </Grid>
          {currentTab === "CityComparison" && (
            <Grid item xs={12} sx={{ px: 4, pb: 4 }}>
              <CityComparisonComponent />
            </Grid>
          )}

          {selectedCities.map(() => {
            const index =
              Math.floor(Math.random() * (TAB_CONTENTS.length - 0)) + 0;
            console.log({ index });
            return (
              <Grid item xs={12}>
                {TAB_CONTENTS[index]}
              </Grid>
            );
          })}
        </Grid>
      </Card>
    </>
  ) : (
    <></>
  );
}

export default DemoOne;
