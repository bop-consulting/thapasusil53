import { useContext } from "react";
import {
  Stack,
  Card,
  Typography,
  Box,
  Divider,
  alpha,
  LinearProgress,
  styled,
  useTheme,
  linearProgressClasses,
  Grid,
} from "@mui/material";

import { useTranslation } from "react-i18next";
import { DemoContext } from "@/contexts/DemoContext";
import SmallCards from "./SmallCards";
import Block7 from "@/content/Blocks/ChartsLarge/Block7";

const LinearProgressPrimary = styled(LinearProgress)(
  ({ theme }) => `
        height: 8px;
        border-radius: ${theme.general.borderRadiusLg};

        &.${linearProgressClasses.colorPrimary} {
            background-color: ${alpha(theme.colors.primary.main, 0.1)};
        }
        
        & .${linearProgressClasses.bar} {
            border-radius: ${theme.general.borderRadiusLg};
            background-color: ${theme.colors.primary.main};
        }
    `
);

const LinearProgressError = styled(LinearProgress)(
  ({ theme }) => `
        height: 8px;
        border-radius: ${theme.general.borderRadiusLg};

        &.${linearProgressClasses.colorPrimary} {
            background-color: ${alpha(theme.colors.error.main, 0.1)};
        }
        
        & .${linearProgressClasses.bar} {
            border-radius: ${theme.general.borderRadiusLg};
            background-color: ${theme.colors.error.main};
        }
    `
);

function CityComparisonComponent() {
  const { t }: { t: any } = useTranslation();
  const theme = useTheme();
  const { selectedCities } = useContext(DemoContext);
  return (
    <Grid container spacing={4}>
      <Grid item xs={12}>
        <Box mb={3}>
          <Typography variant="h3">Cultural Facilities</Typography>
          <Typography variant="subtitle2">Cultural Infrastructure</Typography>
        </Box>
        <Box display="flex" justifyContent="space-between">
          <Typography variant="h3" sx={{ mb: 1, flex: 1 }}>
            {selectedCities[0]?.city_name}
          </Typography>
          <Typography variant="h3" sx={{ mb: 1, flex: 1 }}>
            {selectedCities[1]?.city_name}
          </Typography>
        </Box>
        <Card>
          <Stack
            direction={{ xs: "column", md: "row" }}
            divider={<Divider orientation="vertical" flexItem />}
            justifyContent="space-between"
            alignItems="stretch"
            spacing={0}
          >
            <Box p={2.5} flexGrow={1}>
              <Box
                mb={2}
                display="flex"
                alignItems="center"
                justifyContent="space-between"
              >
                <Box>
                  <Typography color="text.primary" variant="h4" gutterBottom>
                    {t("Libraries")}
                  </Typography>
                  <Typography variant="subtitle2" noWrap>
                    {t("Total libraries to date")}
                  </Typography>
                </Box>
                <Typography
                  variant="h2"
                  sx={{
                    color: `${theme.colors.error.main}`,
                  }}
                >
                  23,594
                </Typography>
              </Box>
              <LinearProgressError variant="determinate" value={66.43} />
              <Box
                display="flex"
                sx={{
                  mt: 0.6,
                }}
                alignItems="center"
                justifyContent="space-between"
              >
                <Typography
                  sx={{
                    color: `${theme.colors.alpha.black[50]}`,
                  }}
                  variant="subtitle2"
                >
                  {t("Target")}
                </Typography>
                <Typography
                  sx={{
                    color: `${theme.colors.alpha.black[50]}`,
                  }}
                  variant="subtitle2"
                >
                  100%
                </Typography>
              </Box>
            </Box>
            <Box p={2.5} flexGrow={1}>
              <Box
                mb={2}
                display="flex"
                alignItems="center"
                justifyContent="space-between"
              >
                <Box>
                  <Typography color="text.primary" variant="h4" gutterBottom>
                    {t("Libraries")}
                  </Typography>
                  <Typography variant="subtitle2" noWrap>
                    {t("Total libraries to date")}
                  </Typography>
                </Box>
                <Typography
                  variant="h2"
                  sx={{
                    color: `${theme.colors.error.main}`,
                  }}
                >
                  $12,346
                </Typography>
              </Box>
              <LinearProgressPrimary variant="determinate" value={44.32} />
              <Box
                display="flex"
                sx={{
                  mt: 0.6,
                }}
                alignItems="center"
                justifyContent="space-between"
              >
                <Typography
                  sx={{
                    color: `${theme.colors.alpha.black[50]}`,
                  }}
                  variant="subtitle2"
                >
                  {t("Target")}
                </Typography>
                <Typography
                  sx={{
                    color: `${theme.colors.alpha.black[50]}`,
                  }}
                  variant="subtitle2"
                >
                  100%
                </Typography>
              </Box>
            </Box>
          </Stack>
        </Card>
      </Grid>
      <Grid item xs={12} sx={{ mt: 3 }}>
        <Box mb={3}>
          <Typography variant="h3">Heritage Sites</Typography>
          <Typography variant="subtitle2">
            A World Heritage Site is a landmark or area with legal protection by
            international convention administered b the United Nations
            Educational, Scientific and Cultural Organization. World Heritage
            Sites are designated by UNESCO for having cultural, historical,
            scientific or other form of significance.
          </Typography>
        </Box>
        <SmallCards />
      </Grid>

      <Grid item xs={12} sx={{ mt: 3 }}>
        <Box mb={3}>
          <Typography variant="h3">Cultural Facilities</Typography>
          <Typography variant="subtitle2">Cultural Infrastructure</Typography>
        </Box>
        <Grid item container spacing={3}>
          <Grid item xs={6}>
            <Block7 title="Paris" subTitle="Cultural Infrastructure" />
          </Grid>
          <Grid item xs={6}>
            <Block7 title="London" subTitle="Cultural Infrastructure" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sx={{ mt: 3 }}>
        <Box mb={3}>
          <Typography variant="h3">Accessibility to Culture</Typography>
          <Typography variant="subtitle2">Paragraph will go here.</Typography>
        </Box>
        <SmallCards />
      </Grid>
    </Grid>
  );
}

export default CityComparisonComponent;
