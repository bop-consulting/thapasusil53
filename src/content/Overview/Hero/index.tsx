import { useContext, useEffect, useRef, useState } from "react";
import Head from "next/head";
import { DemoContext } from "src/contexts/DemoContext";
import { Box, Typography, styled } from "@mui/material";
import Map, { MapRef, Source, Layer } from "react-map-gl";
import { MapData } from "../../../static/GreaterLondon_areaID-3600175342(2)";

const HeroContainer = styled("div")(
  ({ theme }) => `
  height: 85vh;
  position:relative;
  overflow:hidden;
  padding-top: ${theme.spacing(10)};
  padding-bottom: ${theme.spacing(2)};

`
);
const TypographyPrimary = styled(Typography)(
  ({ theme }) => `
    color: ${theme.colors.alpha.trueWhite[100]};  
`
);
const HeroTextContent = styled(Box)`
  position: absolute;
  top: 65vh;
  left: 4rem;
`;

const mapConstZoom = 9;
const initialViewState = {
  longitude: -100,
  latitude: 40,
  zoom: mapConstZoom,
};

function Hero() {
  const mapRef = useRef<MapRef>(null);
  const [mapCordinates] = useState<any>(MapData?.features[0]);
  const { selectedCities } = useContext(DemoContext);

  useEffect(() => {
    if (selectedCities.length) {
      const selectedCity = selectedCities.at(-1);
      mapRef.current?.flyTo({
        center: [selectedCity?.city_longitude, selectedCity?.city_latitude],
        zoom: mapConstZoom,
        duration: 2000,
        speed: 0.2,
        curve: 1,
        easing: (t) => t,
      });
    } else {
      mapRef.current?.flyTo({
        center: [initialViewState.longitude, initialViewState.latitude],
        zoom: mapConstZoom,
        duration: 2000,
        speed: 0.2,
        curve: 1,
        easing: (t) => t,
      });
    }
  }, [selectedCities]);

  return (
    <>
      <Head>
        <link
          href="https://api.mapbox.com/mapbox-gl-js/v2.7.0/mapbox-gl.css"
          rel="stylesheet"
        />
      </Head>
      <HeroContainer>
        <Map
          ref={mapRef}
          scrollZoom={false}
          id="map"
          initialViewState={initialViewState}
          mapStyle="mapbox://styles/dynamite-bud/cl21l6lq4008414lfajomow6s"
          mapboxAccessToken={process.env.NEXT_PUBLIC_MAPBOX_TOKEN}
        >
          {selectedCities && (
            <>
              <Source id="London" type="geojson" data={mapCordinates} />
              <Layer
                id="maine"
                type="fill"
                source="London"
                paint={{
                  "fill-color": "#005263",
                  "fill-opacity": 0.5,
                }}
              />
            </>
          )}
        </Map>
        {selectedCities.length ? (
          <HeroTextContent>
            <TypographyPrimary
              variant="h1"
              sx={(theme) => ({ fontSize: theme.typography.pxToRem(50) })}
            >
              {selectedCities.at(-1)?.city_name}
            </TypographyPrimary>
            <TypographyPrimary
              variant="body2"
              sx={{ fontWeight: "bold", fontSize: "30px" }}
              align="center"
            >
              {selectedCities.at(-1)?.country_name}
            </TypographyPrimary>
          </HeroTextContent>
        ) : (
          ""
        )}
      </HeroContainer>
    </>
  );
}

export default Hero;
