import { gql } from "@apollo/client";

export class CitiesService {
  public constructor() {}

  public GET_ALL_CITIES_QUERY = gql`
    query AllCitiesQuery {
      getAllCities {
        city_id
        city_name
        state_name
        country_name
        city_latitude
        city_longitude
      }
    }
  `;

  public GET_CITY_QUERY = gql`
    query getCity($city: String!) {
      getCity(city: $city) {
        city_name
        country_name
      }
    }
  `;
} 
