import { ICityType } from "@/types/CityType";
import { useState, ReactNode, createContext } from "react";

export interface CityType {
  name: string;
  subTitle: string;
  latitude: number;
  longitude: number;
}

type DemoContext = {
  selectedCities: ICityType[];
  handleCitySelect: (value: ICityType[]) => void;
};

// eslint-disable-next-line @typescript-eslint/no-redeclare
export const DemoContext = createContext<DemoContext>({} as DemoContext);

type Props = {
  children: ReactNode;
};

export function DemoContextProvider({ children }: Props) {
  const [selectedCities, setSelectedCities] = useState<ICityType[]>([]);
  const handleCitySelect = (value: ICityType[]) => {
    setSelectedCities([...value]);
  };

  return (
    <DemoContext.Provider value={{ selectedCities, handleCitySelect }}>
      {children}
    </DemoContext.Provider>
  );
}
