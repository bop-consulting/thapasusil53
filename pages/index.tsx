import { Box, Container, styled } from "@mui/material";
import { ReactElement, useEffect, useMemo } from "react";
import BaseLayout from "src/layouts/BaseLayout";
import { useQuery } from "@apollo/client";

import Head from "next/head";
import Hero from "src/content/Overview/Hero";
import HeaderContent from "src/content/Overview/HeaderContent";
import DemoOne from "src/content/Overview/TabContent/DemoOne";
import React from "react";
import { useDispatch } from "@/store";
import { setAllCities, setLoading } from "@/store/cities";
import { CitiesService } from "@/services/graphql/cities.service";

const HeaderWrapper = styled("div")(
  ({ theme }) => `
  width: 100%;
  display: flex;
  align-items: center;
  height: ${theme.spacing(10)};
  position:absolute;
  top:0;
  z-index:${theme.zIndex.appBar};
`
);

const OverviewWrapper = styled(Box)(
  ({ theme }) => `
    background: ${theme.palette.common.white};
    flex: 1;
    height:100vh;
    overflow-y:scroll;
`
);

function Overview() {
  const citiesService: CitiesService = useMemo(() => new CitiesService(), []);

  const dispatch = useDispatch();

  const { data, loading } = useQuery(citiesService.GET_ALL_CITIES_QUERY);

  useEffect(() => {
    dispatch(setAllCities(data?.getAllCities));
    dispatch(setLoading(loading));
  }, [data]);

  return (
    <OverviewWrapper>
      <Head>
        <title>Demo</title>
      </Head>
      <HeaderWrapper>
        <Container maxWidth="xl">
          <HeaderContent />
        </Container>
      </HeaderWrapper>
      <Hero />
      <DemoOne />
    </OverviewWrapper>
  );
}

export default Overview;

Overview.getLayout = function getLayout(page: ReactElement) {
  return <BaseLayout>{page}</BaseLayout>;
};
