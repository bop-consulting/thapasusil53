import type { AppProps } from "next/app";
import { ApolloProvider } from "@apollo/client";
import Head from "next/head";
import Router from "next/router";
import nProgress from "nprogress";
import "nprogress/nprogress.css";
import ThemeProvider from "src/theme/ThemeProvider";
import CssBaseline from "@mui/material/CssBaseline";
import { CacheProvider, EmotionCache } from "@emotion/react";
import createEmotionCache from "src/createEmotionCache";
import { appWithTranslation } from "next-i18next";
import "src/utils/chart";
import Loader from "src/components/Loader";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import useScrollTop from "src/hooks/useScrollTop";
import { SnackbarProvider } from "notistack";
import { AuthConsumer, AuthProvider } from "src/contexts/JWTAuthContext";
import { DemoContextProvider } from "@/contexts/DemoContext";
import client from "../apollo-client";
import { Provider } from "react-redux";
import store from "../src/store";

const clientSideEmotionCache = createEmotionCache();

// type NextPageWithLayout = NextPage & {
//   getLayout?: (page: ReactElement) => ReactNode;
// };

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
  Component: any;
}

function MyApp(props: MyAppProps) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  const getLayout = Component.getLayout ?? ((page) => page);
  useScrollTop();

  Router.events.on("routeChangeStart", nProgress.start);
  Router.events.on("routeChangeError", nProgress.done);
  Router.events.on("routeChangeComplete", nProgress.done);

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>Demo</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
      </Head>
      <ApolloProvider client={client}>
        <Provider store={store}>
          <DemoContextProvider>
            <ThemeProvider>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <AuthProvider>
                  <SnackbarProvider
                    maxSnack={6}
                    anchorOrigin={{
                      vertical: "bottom",
                      horizontal: "right",
                    }}
                  >
                    <CssBaseline />
                    <AuthConsumer>
                      {(auth) =>
                        !auth.isInitialized ? (
                          <Loader />
                        ) : (
                          getLayout(<Component {...pageProps} />)
                        )
                      }
                    </AuthConsumer>
                  </SnackbarProvider>
                </AuthProvider>
              </LocalizationProvider>
            </ThemeProvider>
          </DemoContextProvider>
        </Provider>
      </ApolloProvider>
    </CacheProvider>
  );
}

export default appWithTranslation(MyApp);
